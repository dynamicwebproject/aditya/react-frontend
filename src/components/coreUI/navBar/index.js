import React from "react";
import { Link, NavLink } from "react-router-dom";
import { useRole } from "../../hooks/useRole";
import { navigations } from "../../../utils/constants";

export const NavBar = () => {
  const { isAdmin, isTeacher, isStudent } = useRole();
  const navs = isAdmin
    ? navigations.ADMIN
    : isTeacher
    ? navigations.TEACHER
    : navigations.STUDENT;
  const logout = () => {
    localStorage.clear();
    window.location.reload();
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-dark sticky-top">
      <Link className="navbar-brand" to="/">
        <img
          className="brandimg-bb"
          src={"/images/logoLMS.png"}
          alt="lms"
          height="40px"
        />
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          {navs.map((nav) => (
            <NavLink
              exact
              className="nav-item nav-link"
              activeClassName="active"
              to={nav.url}
            >
              {nav.displayName}
            </NavLink>
          ))}
        </div>
      </div>
      <div className="form-inline my-2 my-lg-0">
        <button
          className="btn btn-light my-2 my-sm-0 btn-login"
          style={{ color: "S#FE8187" }}
          type="button"
          onClick={logout}
        >
          Logout
        </button>
      </div>
    </nav>
  );
};
