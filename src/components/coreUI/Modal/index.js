import React from "react";

const ModalBox = (props) => {
  const {
    title,
    headerComponent,
    footerComponents,
    id,
    children,
    isStatic,
    onCloseClick,
  } = props;
  return (
    <div
      className="modal fade"
      id={id}
      tabindex="-1"
      aria-labelledby={`${id}Label`}
      aria-hidden="true"
      {...Object.assign(
        {},
        isStatic
          ? { "data-backdrop": "static", "data-keyboard": "false" }
          : null
      )}
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">{title}</h5>
            <button
              id="modalCloser"
              type="button"
              className="btn-close"
              data-dismiss="modal"
              aria-label="Close"
              onClick={onCloseClick}
            ></button>
            {headerComponent}
          </div>
          <div className="modal-body">{children}</div>
          {footerComponents && (
            <div className="modal-footer">{footerComponents}</div>
          )}
        </div>
      </div>
    </div>
  );
};

export default ModalBox;
