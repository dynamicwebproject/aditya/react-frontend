import React from "react";
import ModalBox from ".";

const ConfirmModalBox = ({ toogleConfirmation }) => {
  const footerComponents = (
    <>
      <button
        type="button"
        className="btn btn-primary"
        onClick={() => toogleConfirmation(true)}
        data-dismiss="modal"
      >
        Confirm
      </button>
      <button
        type="button"
        className="btn btn-secondary"
        data-dismiss="modal"
        onClick={() => toogleConfirmation(false)}
      >
        Cancel
      </button>
    </>
  );
  return (
    <ModalBox
      id="confirmationModal"
      title="Confirmation"
      footerComponents={footerComponents}
    >
      <p>Please confirm your action...</p>
    </ModalBox>
  );
};

const LogoutModalBox = () => {
  const footerComponents = (
    <>
      <button
        type="button"
        className="btn btn-primary"
        onClick={() => {
          localStorage.clear();
          window.location.reload();
        }}
      >
        Logout
      </button>
      <button type="button" className="btn btn-secondary" data-dismiss="modal">
        Cancel
      </button>
    </>
  );
  return (
    <ModalBox
      id="logoutModal"
      title="Logout?"
      footerComponents={footerComponents}
      visible={true}
    >
      <p>Please confirm logging out...</p>
    </ModalBox>
  );
};

export { ConfirmModalBox, LogoutModalBox };
