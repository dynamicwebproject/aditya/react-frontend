import React from "react";
import "./styles.css";

export const Input = ({
  handleChange,
  error,
  ref,
  value,
  id,
  placeholder,
  name,
  label,
  type,
  onBlur,
  autoFocus,
  required,
  readOnly,
}) => {
  return (
    <div className="inputWrapper">
      {label && (
        <label htmlFor={id} className="inputLabel">
          {label}
        </label>
      )}
      {error && <span className="inputError">{error}</span>}

      <input
        type={type}
        id={id}
        name={name}
        value={value}
        onChange={handleChange}
        ref={ref}
        placeholder={placeholder}
        onBlur={onBlur}
        autoFocus={autoFocus}
        required={required}
        className="inputElement"
        readOnly={readOnly}
      />
    </div>
  );
};

export const Textarea = ({
  handleChange,
  error,
  ref,
  value,
  id,
  placeholder,
  name,
  label,
  onBlur,
  autoFocus,
  required,
}) => {
  return (
    <div className="inputWrapper">
      {label && (
        <label htmlFor={id} className="inputLabel">
          {label}
        </label>
      )}
      {error && <span className="inputError">{error}</span>}

      <textarea
        id={id}
        name={name}
        value={value}
        onChange={handleChange}
        ref={ref}
        placeholder={placeholder}
        onBlur={onBlur}
        autoFocus={autoFocus}
        required={required}
        className="inputElement textAreaElement"
      />
    </div>
  );
};
