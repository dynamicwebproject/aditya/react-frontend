import React from "react";

export const Footer = () => {
  return (
    <footer className="container pt-1 pb-2">
      <div className="row">
        <div className="col-6 col-md">
          <a className="list-unstyled text-muted" href="#">
            LMS
          </a>
        </div>
        <div className="col-6 col-md">
          <a className="list-unstyled text-muted" href="#">
            FAQ
          </a>
        </div>
        <div className="col-6 col-md">
          <a className="list-unstyled text-muted" href="#">
            Terms
          </a>
        </div>
        <div className="col-6 col-md">
          <a className="list-unstyled text-muted" href="#">
            About Us
          </a>
        </div>
        <div className="col-6 col-md">
          <a className="list-unstyled text-muted" href="#">
            Contact Us
          </a>
        </div>
      </div>
      <div className="row">
        <div className="col-12 col-md">
          <small
            className="d-block mt-3 text-muted"
            style={{ "text-align": "center" }}
          >
            &copy;2021 LMS
          </small>
        </div>
      </div>
    </footer>
  );
};
