import React from "react";

const BadgeText = ({ type = "" }) => {
  switch (type.toUpperCase()) {
    case "SUBMITTED":
      return <span className="badge bg-success">SUBMITTED</span>;
    case "NOTSUBMITTED":
      return <span className="badge bg-warning">NOT-SUBMITTED</span>;
    case "NEW":
      return <span className="badge bg-danger">NEW</span>;
    default:
      return null;
  }
};

export default BadgeText;