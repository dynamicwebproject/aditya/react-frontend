import CryptoJS from "crypto-js";
import { crypto_secret } from "../../utils/constants";
import { UserTypes } from "../../utils/enums";

export const useRole = () => {
  const decrypto = (cyperText) => {
    return CryptoJS.AES.decrypt(cyperText, crypto_secret).toString(
      CryptoJS.enc.Utf8
    );
  };

  const role = decrypto(localStorage.getItem("role") || "");

  const isTeacher = () => role === UserTypes.TEACHER;
  const isAdmin = () => role === UserTypes.ADMIN;
  const isStudent = () => role === UserTypes.STUDENT;

  return {
    isAdmin: isAdmin(),
    isStudent: isStudent(),
    isTeacher: isTeacher(),
    decrypto,
    role,
  };
};
