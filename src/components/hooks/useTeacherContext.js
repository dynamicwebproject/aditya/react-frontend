import React, { createContext, useContext, useEffect, useState } from "react";
import { DataContext } from "./useDataContext";
import { UserContext } from "./useUserContext";
import { groupBy } from "lodash";

export const TeachersContext = createContext();

const initialTeacherData = {
  assignedCourses: [],
};

const TeachersProvider = ({ children }) => {
  const [teacherData, updateTeacherData] = useState(initialTeacherData);
  const { courseList, assignmentList, resourceList } = useContext(DataContext);
  const { userDetail } = useContext(UserContext);

  useEffect(() => {
    if (userDetail && courseList && resourceList && assignmentList) {
      const _assignedCourses = courseList.filter(
        (cl) => cl.teacher === userDetail.teacher_id
      );
      const _assigned = _assignedCourses.map((cl) => {
        return {
          ...cl,
          assignment: assignmentList.filter((al) => al.course === cl.id),
          resource: resourceList.filter((rl) => rl.course === cl.id),
        };
      });
      updateTeacherData((prevState) => ({
        ...prevState,
        assignedCourses: _assigned,
      }));
    }
  }, [userDetail, courseList, assignmentList, resourceList]);

  return (
    <TeachersContext.Provider value={{ ...teacherData }}>
      {children}
    </TeachersContext.Provider>
  );
};

export default TeachersProvider;
