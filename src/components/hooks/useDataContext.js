import React, { createContext, useEffect, useState } from "react";
import { requestGetAllAssignments } from "../../lib/assignmentsApi";
import { requestGetAllCourses } from "../../lib/courseApi";
import { requestGetAllResources } from "../../lib/resourcesApi";
import {
  requestFetchBatches,
  requestGetAllStudents,
} from "../../lib/studentApi";
import { requestGetAllTeachers } from "../../lib/teacherApi";

export const DataContext = createContext();

const initialDataState = {
  batchList: [],
  teacherList: [],
  courseList: [],
  studentList: [],
  assignmentList: [],
  resourceList: [],
  counts: {
    batch: 0,
    teacher: 0,
    course: 0,
    student: 0,
    assignment: 0,
    resource: 0,
  },
};

const DataProvider = ({ children }) => {
  const [data, updateData] = useState(initialDataState);

  const setBatchList = async () => {
    const res = await requestFetchBatches();
    if (res && !res.error) {
      updateData((prevState) => ({
        ...prevState,
        batchList: res,
        counts: { ...prevState.counts, batch: res?.length || 0 },
      }));
    }
  };

  const setStudentList = async () => {
    const res = await requestGetAllStudents();
    if (res && !res.error) {
      updateData((prevState) => ({
        ...prevState,
        studentList: res.data,
        counts: { ...prevState.counts, student: res?.data?.length || 0 },
      }));
    }
  };

  const setTeacherList = async () => {
    const res = await requestGetAllTeachers();
    if (res && !res.error) {
      updateData((prevState) => ({
        ...prevState,
        teacherList: res.data,
        counts: { ...prevState.counts, teacher: res?.data?.length || 0 },
      }));
    }
  };

  const setCourseList = async () => {
    const res = await requestGetAllCourses();
    if (res && !res.error) {
      updateData((prevState) => ({
        ...prevState,
        courseList: res,
        counts: { ...prevState.counts, course: res?.length || 0 },
      }));
    }
  };

  const setAssignmentList = async () => {
    const res = await requestGetAllAssignments();
    if (res && !res.error) {
      updateData((prevState) => ({
        ...prevState,
        assignmentList: res,
        counts: { ...prevState.counts, assignment: res?.length || 0 },
      }));
    }
  };

  const setResourceList = async () => {
    const res = await requestGetAllResources();
    if (res && !res.error) {
      updateData((prevState) => ({
        ...prevState,
        resourceList: res,
        counts: { ...prevState.counts, resource: res?.length || 0 },
      }));
    }
  };

  useEffect(() => {
    setBatchList();
    setTeacherList();
    setCourseList();
    setStudentList();
    setAssignmentList();
    setResourceList();
  }, []);

  return (
    <DataContext.Provider
      value={{
        ...data,
        setBatchList,
        setTeacherList,
        setCourseList,
        setStudentList,
        setAssignmentList,
        setResourceList,
      }}
    >
      {children}
    </DataContext.Provider>
  );
};

export default DataProvider;
