import { useEffect, useState } from "react";
import { getUserDetail } from "../../lib/user";

const useAuth = () => {
  const [userDetail, updateUserDetail] = useState(null);
  const [loading, updateLoading] = useState(true);

  const getAuthStatus = async () => {
    updateLoading(true);
    const userData = await getUserDetail();
    if (userData && userData.data) {
      updateUserDetail(userData.data || null);
    }
    updateLoading(false);
  };

  useEffect(() => {
    getAuthStatus();
  }, [window.location.pathname]);

  return { loading, userDetail, isAuthenticated: !!userDetail };
};

export default useAuth;
