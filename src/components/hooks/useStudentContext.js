import React, { createContext, useContext, useEffect, useState } from "react";
import { DataContext } from "./useDataContext";
import { UserContext } from "./useUserContext";

export const StudentsContext = createContext();

const initialStudentData = {
  assignedCourses: [],
};

const StudentsProvider = ({ children }) => {
  const [studentData, updateStudentData] = useState(initialStudentData);
  const { courseList, assignmentList, resourceList } = useContext(DataContext);
  const { userDetail } = useContext(UserContext);

  useEffect(() => {
    if (userDetail && courseList && resourceList && assignmentList) {
      const _assignedCourses = courseList.filter(
        (cl) => cl.batch === userDetail.user_batch
      );
      const _assigned = _assignedCourses.map((cl) => {
        return {
          ...cl,
          assignment: assignmentList.filter((al) => al.course === cl.id),
          resource: resourceList.filter((rl) => rl.course === cl.id),
        };
      });
      updateStudentData((prevState) => ({
        ...prevState,
        assignedCourses: _assigned,
      }));
    }
  }, [userDetail, courseList, assignmentList, resourceList]);

  return (
    <StudentsContext.Provider value={{ ...studentData }}>
      {children}
    </StudentsContext.Provider>
  );
};

export default StudentsProvider;
