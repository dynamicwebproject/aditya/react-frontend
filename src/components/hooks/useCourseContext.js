import React, { FC, createContext, useState } from "react";

export const CoursesContext = createContext();

const CoursesProvider = ({ children }) => {
  const [coursesList, updateCoursesList] = useState(null);

  const setCoursesList = (detail) => {
    if (!coursesList) updateCoursesList(detail);
  };

  return (
    <CoursesContext.Provider value={{ coursesList, setCoursesList }}>
      {children}
    </CoursesContext.Provider>
  );
};

export default CoursesProvider;
