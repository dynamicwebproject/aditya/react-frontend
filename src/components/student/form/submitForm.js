import React, { useState } from "react";
import { toast } from "react-toastify";
import { requestAssignmentSubmit } from "../../../lib/studentApi";
import { Input, Textarea } from "../../coreUI";

const initialFormState = {
  title: "",
  description: "",
};

export const SubmitAssignmentForm = ({ author, assignment }) => {
  const [formState, setFormState] = useState(initialFormState);
  const [errorState, setErrorState] = useState(initialFormState);

  const closeModal = () => {
    setFormState(initialFormState);
    setErrorState(initialFormState);
    document.getElementById("modalCloser").click();
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validate()) {
      const res = await requestAssignmentSubmit({
        ...formState,
        author,
        assignment,
      });
      if (!res || res.error) {
        toast.error(res?.error || "");
      } else {
        toast.success("Assignment submitted successfully.");
        closeModal();
      }
    }
  };

  const validate = () => {
    let _errors = {};
    if (!formState.title) _errors.title = "Title is required.";
    if (!formState.description)
      _errors.description = "Description is required.";
    setErrorState(_errors);
    return Object.keys(_errors).length === 0;
  };

  return (
    <form onSubmit={handleSubmit}>
      <Input
        type="text"
        name="title"
        placeholder="First Name"
        value={formState.title}
        error={errorState.title}
        handleChange={handleChange}
      />
      <Textarea
        name="description"
        placeholder="Detail of submittion"
        value={formState.description}
        error={errorState.description}
        handleChange={handleChange}
      />
      <button
        className="btn btn-primary"
        type="submit"
        disabled={() => validate()}
      >
        Submit Assignment
      </button>
    </form>
  );
};
