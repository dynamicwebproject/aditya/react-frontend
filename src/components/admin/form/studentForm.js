import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { requestSendResetPasswordMail } from "../../../lib/authApi";
import {
  requestAddStudent,
  requestUpdateStudent,
} from "../../../lib/studentApi";
import { Input } from "../../coreUI";

const initialFormState = {
  first_name: "",
  last_name: "",
  email: "",
  username: "",
  user_type: 3,
  user_batch: "",
};

export const AdminStudentForm = ({ updateData, onComplete, batches = [] }) => {
  const [formState, setFormState] = useState(initialFormState);
  const [errorState, setErrorState] = useState(initialFormState);
  const [disableSubmit, setDisableSubmit] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);

  let isUpdate = !!updateData;

  useEffect(() => {
    isUpdate = !!updateData;
    if (isUpdate) {
      Object.keys(formState).map((key) => {
        setFormState((prevState) => ({
          ...prevState,
          [key]: updateData[key] || formState[key],
        }));
      });
    } else {
      setFormState(initialFormState);
    }
  }, [updateData]);

  const closeModal = () => {
    setFormState(initialFormState);
    setErrorState(initialFormState);
    onComplete();
    document.getElementById("modalCloser").click();
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState((prevState) => ({ ...prevState, [name]: value }));
    setDisableSubmit(false);
    setErrorState((prevState) => ({ ...prevState, [name]: "" }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setDisableSubmit(true);
    if (validate()) {
      setIsSubmitting(true);
      let res;
      if (isUpdate) {
        res = await requestUpdateStudent(formState, updateData.id);
      } else {
        const initialRes = await requestAddStudent(formState);
        if (!initialRes || initialRes.error) {
          res = initialRes;
        } else {
          res = await requestSendResetPasswordMail(formState.email);
        }
      }
      if (!res || res.error) {
        toast.error(res?.error || "");
      } else {
        toast.success("Successful");
        closeModal();
      }
      setIsSubmitting(false);
      setDisableSubmit(false);
    }
  };

  const validate = () => {
    let _errors = {};
    if (!formState.first_name) _errors.first_name = "First name is required.";
    if (!formState.last_name) _errors.last_name = "Last name is required.";
    if (!formState.email) _errors.email = "Email Address is required.";
    if (!formState.username) _errors.username = "Username is required.";
    setErrorState(_errors);
    return Object.keys(_errors).length === 0;
  };

  return (
    <form onSubmit={handleSubmit}>
      <select
        name="user_batch"
        onChange={handleChange}
        value={formState.user_batch}
        required
        className="inputElement"
      >
        <option value="">- Batch -</option>
        {batches.map((btc) => (
          <option value={btc.id} key={btc.id}>
            {btc.batch}
          </option>
        ))}
      </select>
      <Input
        type="text"
        name="first_name"
        placeholder="First Name"
        value={formState.first_name}
        error={errorState.first_name}
        handleChange={handleChange}
      />
      <Input
        type="text"
        name="last_name"
        placeholder="Last Name"
        value={formState.last_name}
        error={errorState.last_name}
        handleChange={handleChange}
      />
      <Input
        type="text"
        name="email"
        placeholder="Email Address"
        value={formState.email}
        error={errorState.email}
        readOnly={isUpdate}
        handleChange={handleChange}
      />
      <Input
        type="text"
        name="username"
        placeholder="Username"
        value={formState.username}
        error={errorState.username}
        readOnly={isUpdate}
        handleChange={handleChange}
      />
      <button
        className="btn btn-primary"
        type="submit"
        disabled={isSubmitting || disableSubmit}
      >
        {isUpdate ? "Update Student" : "Add New Student"}
      </button>
    </form>
  );
};
