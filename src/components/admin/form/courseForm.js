import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { requestAddCourse, requestUpdateCourse } from "../../../lib/courseApi";
import { Input, Textarea } from "../../coreUI";

const initialFormState = {
  code: "",
  title: "",
  description: "",
  batch: "",
  teacher: "",
};

export const AdminCourseForm = ({
  updateData,
  onComplete,
  batches = [],
  teachers = [],
}) => {
  const [formState, setFormState] = useState(initialFormState);
  const [errorState, setErrorState] = useState(initialFormState);

  let isUpdate = !!updateData;

  useEffect(() => {
    isUpdate = !!updateData;
    if (isUpdate) {
      Object.keys(formState).map((key) => {
        setFormState((prevState) => ({
          ...prevState,
          [key]: updateData[key] || formState[key],
        }));
      });
    } else {
      setFormState(initialFormState);
    }
  }, [updateData]);

  const closeModal = () => {
    setFormState(initialFormState);
    setErrorState(initialFormState);
    onComplete();
    document.getElementById("modalCloser").click();
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validate()) {
      let res;
      if (isUpdate) {
        res = await requestUpdateCourse(formState, updateData.id);
      } else {
        res = await requestAddCourse(formState);
      }
      if (!res || res.error) {
        toast.error(res.error);
      } else {
        toast.success("Successful");
        closeModal();
      }
    }
  };

  const validate = () => {
    let _errors = {};
    if (!formState.batch) _errors.batch = "Course batch is required.";
    if (!formState.teacher) _errors.teacher = "Course teacher is required.";
    if (!formState.code) _errors.code = "Course code is required.";
    if (!formState.title) _errors.title = "Course title is required.";
    if (!formState.description)
      _errors.description = "Description is required.";
    setErrorState(_errors);
    return Object.keys(_errors).length === 0;
  };

  return (
    <form onSubmit={handleSubmit}>
      <select
        name="batch"
        onChange={handleChange}
        value={formState.batch}
        required
        className="inputElement w-50"
      >
        <option value="" disabled>
          - Select Batch -
        </option>
        {batches &&
          batches.length > 0 &&
          batches.map((btc) => (
            <option value={btc.id} key={btc.id}>
              {btc.batch}
            </option>
          ))}
      </select>
      <select
        name="teacher"
        onChange={handleChange}
        value={formState.teacher}
        required
        className="inputElement mt-3 w-50"
      >
        <option value="" disabled>
          - Select Teacher -
        </option>
        {teachers &&
          teachers.length > 0 &&
          teachers.map((tch) => (
            <option value={tch.id} key={tch.id}>
              {tch.last_name}, {tch.user__username}
            </option>
          ))}
      </select>
      <Input
        type="text"
        name="code"
        placeholder="Course code"
        value={formState.code}
        error={errorState.code}
        handleChange={handleChange}
      />
      <Input
        type="text"
        name="title"
        placeholder="Title"
        value={formState.title}
        error={errorState.title}
        handleChange={handleChange}
      />
      <Textarea
        type="text"
        name="description"
        placeholder="Description of the course"
        value={formState.description}
        error={errorState.description}
        handleChange={handleChange}
      />
      <button
        className="btn btn-primary"
        type="submit"
        disabled={() => validate()}
      >
        {isUpdate ? "Update Course" : "Add New Course"}
      </button>
    </form>
  );
};