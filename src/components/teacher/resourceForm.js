import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import {
  requestAddResource,
  requestUpdateResource,
} from "../../lib/resourcesApi";
import { Input, Textarea } from "../coreUI";

const initialFormState = {
  title: "",
  description: "",
};

export const TeacherResourceForm = ({
  updateData,
  onComplete,
  course = "",
}) => {
  const [formState, setFormState] = useState(initialFormState);
  const [errorState, setErrorState] = useState(initialFormState);

  let isUpdate = !!updateData;

  useEffect(() => {
    isUpdate = !!updateData;
    if (isUpdate) {
      Object.keys(formState).map((key) => {
        setFormState((prevState) => ({
          ...prevState,
          [key]: updateData[key] || formState[key],
        }));
      });
    } else {
      setFormState(initialFormState);
    }
  }, [updateData]);

  const closeModal = () => {
    setFormState(initialFormState);
    setErrorState(initialFormState);
    onComplete();
    document.getElementById("modalCloser").click();
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validate()) {
      let res;
      if (isUpdate) {
        res = await requestUpdateResource(
          { ...formState, course },
          updateData.id
        );
      } else {
        res = await requestAddResource({ ...formState, course });
      }
      if (!res || res.error) {
        toast.error(res.error);
      } else {
        toast.success("Successful");
        closeModal();
      }
    }
  };

  const validate = () => {
    let _errors = {};
    if (!formState.title) _errors.title = "Course title is required.";
    if (!formState.description)
      _errors.description = "Description is required.";
    setErrorState(_errors);
    return Object.keys(_errors).length === 0;
  };

  return (
    <form onSubmit={handleSubmit}>
      <Input
        type="text"
        name="title"
        placeholder="Title"
        value={formState.title}
        error={errorState.title}
        handleChange={handleChange}
      />
      <Textarea
        type="text"
        name="description"
        placeholder="Description of the course"
        value={formState.description}
        error={errorState.description}
        handleChange={handleChange}
      />
      <button
        className="btn btn-primary"
        type="submit"
        disabled={() => validate()}
      >
        {isUpdate ? "Update Assignment" : "Add New Assignment"}
      </button>
    </form>
  );
};
