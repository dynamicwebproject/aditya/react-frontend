import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import {
  requestAddAssignment,
  requestUpdateAssignment,
} from "../../lib/assignmentsApi";
import { Input, Textarea } from "../coreUI";

const initialFormState = {
  title: "",
  description: "",
  due_date: "",
};

export const TeacherAssignmentForm = ({
  updateData,
  onComplete,
  course = "",
}) => {
  const [formState, setFormState] = useState(initialFormState);
  const [errorState, setErrorState] = useState(initialFormState);

  let isUpdate = !!updateData;

  useEffect(() => {
    isUpdate = !!updateData;
    if (isUpdate) {
      Object.keys(formState).map((key) => {
        let value = updateData[key] || formState[key];
        if (key === "due_date") {
          value = updateData[key].split("T")[0];
        }
        setFormState((prevState) => ({
          ...prevState,
          [key]: value,
        }));
      });
    } else {
      setFormState(initialFormState);
    }
  }, [updateData]);

  const closeModal = () => {
    setFormState(initialFormState);
    setErrorState(initialFormState);
    onComplete();
    document.getElementById("modalCloser").click();
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validate()) {
      const { due_date, ...remainingState } = formState;
      const updatedDate = `${due_date}T23:59`;
      let res;
      if (isUpdate) {
        res = await requestUpdateAssignment(
          { ...remainingState, due_date: updatedDate, course },
          updateData.id
        );
      } else {
        res = await requestAddAssignment({
          ...remainingState,
          due_date: updatedDate,
          course,
        });
      }
      if (!res || res.error) {
        toast.error(res.error);
      } else {
        toast.success("Successful");
        closeModal();
      }
    }
  };

  const validate = () => {
    let _errors = {};
    if (!formState.title) _errors.title = "Course title is required.";
    if (!formState.description)
      _errors.description = "Description is required.";
    if (!formState.due_date) _errors.due_date = "Due date is required.";
    setErrorState(_errors);
    return Object.keys(_errors).length === 0;
  };

  return (
    <form onSubmit={handleSubmit}>
      <Input
        type="text"
        name="title"
        placeholder="Title"
        value={formState.title}
        error={errorState.title}
        handleChange={handleChange}
      />
      <Textarea
        type="text"
        name="description"
        placeholder="Description of the course"
        value={formState.description}
        error={errorState.description}
        handleChange={handleChange}
      />
      <Input
        type="date"
        name="due_date"
        value={formState.due_date}
        error={errorState.due_date}
        handleChange={handleChange}
      />
      <button
        className="btn btn-primary"
        type="submit"
        disabled={() => validate()}
      >
        {isUpdate ? "Update Assignment" : "Add New Assignment"}
      </button>
    </form>
  );
};
