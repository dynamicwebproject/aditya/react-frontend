import React from "react";
import AppRouting from "./route";
import { BrowserRouter } from "react-router-dom";
import { ToastContainer } from "react-toastify";
//CSS
import "./App.css";
import "react-toastify/dist/ReactToastify.css";
import UserProvider from "./components/hooks/useUserContext";
import DataProvider from "./components/hooks/useDataContext";
import TeachersProvider from "./components/hooks/useTeacherContext";

function App() {
  return (
    <div>
      <BrowserRouter>
        <ToastContainer />
        <UserProvider>
          <DataProvider>
            <AppRouting />
          </DataProvider>
        </UserProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
