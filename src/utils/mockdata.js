const enrolledCourses = [
  {
    id: "dojasdlkjasd",
    name: "Developing Modern Web",
    teacher: "Pramod Khanal",
    courseRole: "Lecturer",
    overview:
      "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
    assignment: [
      {
        title: "Assignment title one",
        description:
          "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
        startDate: "12 Nov 2020",
        dueDate: "12 Jan 2021",
      },
      {
        title: "Assignment title two",
        description:
          "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
        startDate: "12 Nov 2020",
        dueDate: "12 Jan 2021",
      },
    ],
    resources: [
      {
        resourceId: "0sldkfnsdf",
        title: "Tile of the resource one",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
      {
        resourceId: "0sldkfnsaf",
        title: "Tile of the resource two",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
      {
        resourceId: "0sldsdnsdf",
        title: "Tile of the resource three",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
      {
        resourceId: "0sledfnsdf",
        title: "Tile of the resource four",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
    ],
  },
  {
    id: "oo3i1klasnda",
    name: "Developing Modern Web",
    teacher: "Pramod Khanal",
    courseRole: "Lecturer",
    overview:
      "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
    assignment: [
      {
        title: "Assignment title one",
        description:
          "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
        startDate: "12 Nov 2020",
        dueDate: "12 Jan 2021",
      },
    ],
    resources: [
      {
        resourceId: "0sldk22sdf",
        title: "Tile of the resource one",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
      {
        resourceId: "0sld4gnsdf",
        title: "Tile of the resource two",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
      {
        resourceId: "0sldk2dsdf",
        title: "Tile of the resource three",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
      {
        resourceId: "12ldkfnsdf",
        title: "Tile of the resource four",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
    ],
  },
  {
    id: "a09s8djaoskl",
    name: "Developing Modern Web",
    teacher: "Pramod Khanal",
    courseRole: "Lecturer",
    overview:
      "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
    assignment: [
      {
        title: "Assignment title one",
        description:
          "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
        startDate: "12 Nov 2020",
        dueDate: "12 Jan 2021",
      },
    ],
    resources: [
      {
        resourceId: "02ldkf4sdf",
        title: "Tile of the resource one",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
      {
        resourceId: "0s5dkf2sdf",
        title: "Tile of the resource two",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
      {
        resourceId: "0slskfnsgf",
        title: "Tile of the resource three",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
      {
        resourceId: "0slwkfnsdf",
        title: "Tile of the resource four",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
    ],
  },
  {
    id: "foij0s9djds0",
    name: "Developing Modern Web",
    teacher: "Pramod Khanal",
    courseRole: "Lecturer",
    overview:
      "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
    assignment: [
      {
        title: "Assignment title one",
        description:
          "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
      },
    ],
    resources: [
      {
        resourceId: "0sldefnsdf",
        title: "Tile of the resource one",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
      {
        resourceId: "0sldk7ssdf",
        title: "Tile of the resource two",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
      {
        resourceId: "0sledfnsdf",
        title: "Tile of the resource three",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
      {
        resourceId: "0sl9wfnsdf",
        title: "Tile of the resource four",
        content:
          "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
      },
    ],
  },
];

const assignment = [
  {
    title: "Assignment title one",
    description:
      "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
    startDate: "12 Nov 2020",
    dueDate: "12 Jan 2021",
  },
  {
    title: "Assignment title two",
    description:
      "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
    startDate: "12 Nov 2020",
    dueDate: "12 Jan 2021",
  },
];
const resources = [
  {
    resourceId: "0sldkfnsdf",
    title: "Tile of the resource one",
    content:
      "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
  },
  {
    resourceId: "0sldkfnsaf",
    title: "Tile of the resource two",
    content:
      "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
  },
  {
    resourceId: "0sldsdnsdf",
    title: "Tile of the resource three",
    content:
      "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
  },
  {
    resourceId: "0sledfnsdf",
    title: "Tile of the resource four",
    content:
      "Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi Lorem muspi ",
  },
];

export { enrolledCourses, assignment, resources };
