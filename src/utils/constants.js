const meta = {
  description: "Learn about your LMS",
};

const errors = {
  generic: "Something went Wrong",
  login: {
    email: {
      required: "Email field is required.",
      invalid: "Input must be a valid email address.",
    },
    password: {
      required: "Password field is required.",
      min: "Password must be at least 8 characters.",
    },
  },
  profile: {
    password: {
      mismatch: "Passwords do not match",
      invalid: "Invalid Password",
    },
    userDetails: {
      firstName: "First Name is required.",
      lastName: "Last Name is required.",
      age: "Age is required.",
      gender: "Gender is required.",
      country: "Country is required.",
    },
  },
  survey: {
    invalidEmail: "Invalid Email",
    localDataUnavailable: "Local Survey data not available",
    not_found_redirect: "Survey not available. Redirected to primary survey.",
  },
};

const labels = {
  tutorial: {
    options: "Options",
    question: "Question",
    prev: "Prev Slide",
    next: "Next Slide",
    complete: "Got it",
  },
  landing: {
    sidebarText: {
      line1: `Let's get`,
      line2: "started!",
    },
  },
  auth: {
    title: "Create your account",
    desc: {
      line1: "Please create an account to see the results.",
      line2: "Connect your social account for one-click sign in:",
    },
    dividerText: "or",
    hasAccount: "Already have an account?",
    email: "Email Address",
    password: "Password",
    sidebarText: {
      line1: `Let's`,
      line2: "finalize it!",
    },
    login: "Log In",
    logout: "Log out",
    resetPassword: "Reset Password",
    newPassword: "Your new password",
    confirmPassword: "Confirm your password",
  },
  survey: {
    startSurvey: "Start Survey",
    completeText: "Complete Survey",
    emailInput: "Enter your email",
    seeResults: "See Results",
    login: "Login",
    googleLogin: "Login with Google",
    facebookLogin: "Login With Facebook",
    start: "Start",
    back: "Back",
    next: "Next",
    view: "View Results",
    skip: "Skip",
  },
  home: {
    welcome: "Welcome Home",
  },
  profile: {
    password: "Password",
    reenterPassword: "Re-enter Password",
    setPassword: "Update Password",
    passwordStrength: {
      strong: "strong",
      medium: "medium",
      enough: "easy",
    },
    details: {
      firstName: "First Name",
      lastName: "Last Name",
      age: "Age",
      gender: "Gender",
      country: "What is your country of origin?",
      ethnicity: "What is your race/ethnicity?",
      education: "Highest level of education completed?",
    },
    setName: "Set Names",
    next: "Next",
    showResult: "Show me my results",
  },
};

const placeholders = {
  auth: {
    email: "Enter your email",
    password: "Enter your password",
    newPassword: "Your new password",
    confirmPassword: "Confirm your new password",
  },
  profile: {
    details: {
      firstName: "First Name",
      lastName: "Last Name",
      age: "Your Age",
      gender: "Your Gender",
      country: "Country",
      ethnicity: "Ethnicity",
      education: "Your highest level degree",
    },
  },
};

const successes = {
  auth: {
    login: "You have been logged in.",
  },
};

const crypto_secret = "oi13nlk12ni12lkns09jad1oin12";

const navigations = {
  ADMIN: [
    {
      displayName: "Dashboard",
      url: "/admin/dashboard",
    },
    {
      displayName: "Manage Teachers",
      url: "/admin/teachers",
    },
    {
      displayName: "Manage Students",
      url: "/admin/students",
    },
    {
      displayName: "Manage Courses",
      url: "/admin/courses",
    },
  ],
  TEACHER: [
    {
      displayName: "Dashboard",
      url: "/teacher/dashboard",
    },
    {
      displayName: "Assign Courses",
      url: "/teacher/courses",
    },
    // {
    //   displayName: "Manage Assignment",
    //   url: "/teacher/assignments",
    // },
  ],
  STUDENT: [
    {
      displayName: "Dashboard",
      url: "/dashboard",
    },
    {
      displayName: "Your Courses",
      url: "/courses",
    },
    // {
    //   displayName: "Your Assignments",
    //   url: "/assignments",
    // },
  ],
};

export { labels, errors, meta, placeholders, successes, crypto_secret, navigations };
