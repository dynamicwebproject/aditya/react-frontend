const LoginFormTypes = {
  username: "username",
  password: "password",
};

const UserTypes = {
  ADMIN: 'Admin',
  TEACHER: 'Teacher',
  STUDENT: 'Student',
}

export { LoginFormTypes, UserTypes };
