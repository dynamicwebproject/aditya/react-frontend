import React, { useState } from "react";
import { toast } from "react-toastify";
import { Input } from "../../components/coreUI";
import { requestResetPassword } from "../../lib/authApi";
import { errors } from "../../utils/constants";
import { validatePassword } from "../../utils/validations";

const initialFormState = {
  confirmPassword: { value: "", touched: false },
  password: { value: "", touched: false },
};

const initialErrorState = {
  confirmPassword: "",
  password: "",
};

const ResetPassword = (props) => {
  const [formState, setFormState] = useState(initialFormState);
  const [errorState, setErrorState] = useState(initialErrorState);
  const [passwordStrength, setPasswordStrength] = useState("");
  const [error, setError] = useState("");

  const token = props.match.params.token || "";

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState((prevState) => ({
      ...prevState,
      [name]: { ...prevState[name], value },
    }));
    if (name === "password" && formState.password.touched) validateStrength(e);
    if (formState.confirmPassword.touched) matchPasswords(e);
  };

  const setTouched = (x) => {
    setFormState((prevState) => ({
      ...prevState,
      [x]: { ...prevState[x], touched: true },
    }));
  };

  const validateStrength = (e) => {
    if (!formState.password.touched) setTouched("password");
    const { value } = e.target;
    const strength = validatePassword(value);
    if (!strength) {
      setErrorState((prevState) => ({
        ...prevState,
        password: "Invalid Password",
      }));
      setPasswordStrength("");
    } else {
      setErrorState((prevState) => ({ ...prevState, password: "" }));
      setPasswordStrength(strength);
    }
  };

  const matchPasswords = (e) => {
    if (!formState.confirmPassword.touched) setTouched("confirmPassword");
    const { value } = e.target;
    setErrorState((prevState) => ({
      ...prevState,
      confirmPassword:
        value !== formState.password.value ? "Passwords does not matched" : "",
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const response = await requestResetPassword({
      token,
      password: formState.password.value,
    });
    if (!response || response.error) {
      setError(response.error || errors.generic);
    } else {
      toast.success("Password reset successful.");
      props.history.push("/login");
    }
  };

  const disableSubmitButton = () =>
    !formState.password.value ||
    !formState.confirmPassword.value ||
    errorState.password ||
    errorState.confirmPassword;

  return (
    <>
      <h1 className="nonAuthHeadings">Set your new password</h1>
      <form onSubmit={handleSubmit}>
        <Input
          type="password"
          name="password"
          label="New Password"
          handleChange={handleChange}
          value={formState.password.value}
          error={errorState.password}
          placeholder="New Password"
          onBlur={validateStrength}
        />
        <Input
          type="password"
          name="confirmPassword"
          label="Confirm Password"
          handleChange={handleChange}
          value={formState.confirmPassword.value}
          error={errorState.confirmPassword}
          placeholder="Confirm Password"
          onBlur={matchPasswords}
        />
        <div>
          Strength: <strong>{passwordStrength}</strong>
        </div>
        <button
          type="submit"
          className="btn btn-primary w-50"
          disabled={disableSubmitButton()}
        >
          Reset Password
        </button>
        <input type="reset" className="btn btn-warning w-50" value="Reset" />
      </form>
    </>
  );
};

export default ResetPassword;
