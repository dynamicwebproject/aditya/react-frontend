import React, { useState } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import { Input } from "../../components/coreUI";
import { requestLogin } from "../../lib/authApi";
import { errors, successes } from "../../utils/constants";

const initialFormState = {
  username: "",
  password: "",
};

const initialErrorState = {
  username: "",
  password: "",
};

const Login = (props) => {
  const [formState, setFormState] = useState(initialFormState);
  const [errorState, setErrorState] = useState(initialErrorState);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState((prevState) => ({ ...prevState, [name]: value }));
    setErrorState(initialErrorState);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const response = await requestLogin({
      username: formState.username,
      password: formState.password,
    });
    if (!response || response.error) {
      setErrorState({ username: response.error, password: response.error });
    } else {
      toast.success(successes.auth.login);
      window.location.reload();
    }
  };

  const disableSubmitButton = () =>
    !formState.password ||
    !formState.username ||
    errorState.password ||
    errorState.username;

  return (
    <>
    <h1 className="nonAuthHeadings">Login your account</h1>
    <form onSubmit={handleSubmit}>
      <Input
        autoFocus={true}
        type="text"
        placeholder="Username"
        label="Username"
        name="username"
        handleChange={handleChange}
        value={formState.username}
        error={errorState.username}
      />
      <Input
        type="password"
        placeholder="Password"
        label="Password"
        name="password"
        handleChange={handleChange}
        value={formState.password}
        error={errorState.password}
      />
      <button type="submit" className="btn btn-primary d-block w-50 mx-auto my-3" disabled={disableSubmitButton()}>
        Submit
      </button>
      <Link className="d-block mx-auto text-center " to="/forgot-password">Forgot password ?</Link>
    </form>
    </>
  );
};

export default Login;
