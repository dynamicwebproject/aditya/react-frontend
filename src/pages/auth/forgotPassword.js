import React, { useState } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import { Input } from "../../components/coreUI";
import { requestSendResetPasswordMail } from "../../lib/authApi";
import { errors } from "../../utils/constants";

const ForgotPassword = (props) => {
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");

  const handleChange = (e) => {
    const { value } = e.target;
    setEmail(value);
    setError("");
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const response = await requestSendResetPasswordMail(email);
    if (!response || response.error) {
      setError(response.error || errors.generic);
    } else {
      toast.success("Reset link sent to email.");
      props.history.push("/login");
    }
  };

  const disableSubmitButton = () => !email || error;

  return (
    <>
      <h1 className="nonAuthHeadings">Provide your email</h1>
      <form onSubmit={handleSubmit}>
        <Input
          type="email"
          name="email"
          handleChange={handleChange}
          value={email}
          error={error}
        />
        <button
          type="submit"
          className="btn btn-primary w-50"
          disabled={disableSubmitButton()}
        >
          Reset Password
        </button>
        <input type="reset" className="btn btn-warning w-50" value="Reset" />
        <Link to="/">Back to Login</Link>
      </form>
    </>
  );
};

export default ForgotPassword;
