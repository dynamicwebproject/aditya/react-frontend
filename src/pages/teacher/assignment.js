import React from "react";
import { enrolledCourses } from "../../utils/mockdata";

export const TeacherAssignment = (props) => {
  const courseId = props.match.params.id;
  const resourceId = props.match.params.resourceId;
  const course = enrolledCourses.filter((ec) => ec.id === courseId)[0];
  const resource = course.resources.filter(
    (r) => r.resourceId === resourceId
  )[0];
  return (
    <>
      <div className="mx-5 my-5 text-left">
        <h1 className="">{course.name}</h1>
        <h4 className="">
          {course.courseRole}: {course.teacher}
        </h4>
      </div>

      <div className="px-5">
        <div
          className="mx-5 my-5 row align-items-center p-1 IndivCourseElement1"
          style={{ "background-color": "white", "border-radius": "25px" }}
        >
          <div
            className="col-lg-12 col-sm-12 text-left mx-2 px-3"
            style={{ "min-height": "500px" }}
          >
            <h1 className="mb-2 mt-3">Lecture: {resource.title}</h1>
            <hr />
            <p className="mb-5 indivCourseOverviewText">{resource.content}</p>
          </div>
        </div>
      </div>
    </>
  );
};
