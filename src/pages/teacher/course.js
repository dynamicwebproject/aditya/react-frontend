import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import { DataContext } from "../../components/hooks/useDataContext";
import { TeachersContext } from "../../components/hooks/useTeacherContext";
import { TeacherAssignmentForm } from "../../components/teacher/assignmentForm";
import { TeacherResourceForm } from "../../components/teacher/resourceForm";
import { requestDeleteAssignment } from "../../lib/assignmentsApi";
import { requestDeleteResource } from "../../lib/resourcesApi";
import ModalBox from "../../components/coreUI/Modal/index";
import { ConfirmModalBox } from "../../components/coreUI/Modal/PredefineModals";

const initialToDeleteState = {
  type: "",
  id: -1,
};

const RESOURCE = "resource";
const ASSIGNMENT = "assignment";

export const TeacherCoursePage = (props) => {
  const courseId = props.match.params.id;
  const { assignedCourses } = useContext(TeachersContext);
  const { setAssignmentList, setResourceList } = useContext(DataContext);
  const [toDelete, setToDelete] = useState(initialToDeleteState);
  const [updateResourceData, setUpdateResourceData] = useState(null);
  const [updateAssignmentData, setUpdateAssignmentData] = useState(null);

  const [course, setCourse] = useState(null);

  useEffect(() => {
    if (assignedCourses && assignedCourses.length > 0) {
      setCourse(
        assignedCourses.filter((ec) => ec.id === parseInt(courseId))[0]
      );
    }
  }, [assignedCourses]);

  const handleEdit = (e) => {
    const { value, name } = e.target;
    const data = course[name].filter((ass) => ass.id.toString() === value)[0];
    if (name === ASSIGNMENT) {
      setUpdateAssignmentData({
        title: data.title,
        description: data.description,
        due_date: data.due_date,
        id: data.id,
      });
    }
    if (name === RESOURCE) {
      setUpdateResourceData({
        title: data.title,
        description: data.description,
        id: data.id,
      });
    }
  };

  const handleAddNew = () => {
    setUpdateAssignmentData(null);
    setUpdateResourceData(null);
  };

  const toogleConfirmation = (val) => {
    if (val) {
      handleDelete();
    }
  };

  const handleDelete = async () => {
    let res = null;
    if (toDelete.type === ASSIGNMENT) {
      res = await requestDeleteAssignment(toDelete.id);
    } else {
      res = await requestDeleteResource(toDelete.id);
    }
    if (!res || res.error) {
      toast.error(res?.error);
    } else {
      if (toDelete.type === ASSIGNMENT) {
        setAssignmentList();
      }
      if (toDelete.type === RESOURCE) {
        console.log("here 2222s");
        setResourceList();
      }
      toast.success("Successful");
      setToDelete(initialToDeleteState);
    }
  };

  return (
    <>
      {course && (
        <>
          <div className="mx-5 my-5 text-left">
            <h1 className="">{course.title}</h1>
            <h4 className="">
              <span className="text-muted">Batch:</span> {course.batch}
            </h4>
          </div>
          <div
            className="mx-5 my-5 row align-items-center p-1 IndivCourseElement1"
            style={{ "background-color": "white", "border-radius": "25px" }}
          >
            <div className="col-lg-12 col-sm-12 text-left mx-2 px-3">
              <h1 className="mb-2 mt-3">Overview</h1>
              <hr />
              <p className="mb-5 indivCourseOverviewText">
                {course.description}
              </p>
            </div>
          </div>
          <div className="mx-0 my-5 row align-items-center pb-5 indivCourseElement2">
            <div className="col-lg-12 col-sm-12 text-left mx-2 px-3 pt-3">
              <div className="px-4 row justify-content-between">
                <h1 className="mb-2 mt-3 text-white">
                  Assignments
                  <button
                    type="button"
                    className="btn btn-warning mx-4"
                    data-toggle="modal"
                    data-target="#manageAssignmentForm"
                    onClick={handleAddNew}
                  >
                    <span>+</span> Add New
                  </button>
                </h1>
              </div>
              <hr className="mb-4" style={{ "background-color": "white" }} />
              <div className="row align-items-center">
                {course?.assignment &&
                  course?.assignment.length > 0 &&
                  course?.assignment?.map((ca) => (
                    <div className="col-lg-3 col-sm-3 mx-5 text-left p-2 px-3 mb-4 indivCourseAssignment">
                      <h3 className="mb-2 mt-3">{ca.title}</h3>
                      <button
                        type="button"
                        className="btn btn-sm btn-info w-50"
                        data-toggle="modal"
                        data-target="#manageAssignmentForm"
                        name={ASSIGNMENT}
                        onClick={handleEdit}
                        value={ca.id}
                      >
                        Edit
                      </button>
                      <button
                        type="button"
                        className="btn btn-sm btn-outline-danger w-50"
                        data-toggle="modal"
                        data-target="#confirmationModal"
                        onClick={() =>
                          setToDelete({ type: ASSIGNMENT, id: ca.id })
                        }
                      >
                        Delete
                      </button>
                      <p className="mb-3 indivCourseDescription indivCourseOverviewText">
                        {ca.description}
                      </p>
                      <p className="mb-3 indivCourseOverviewText">
                        <span className="text-muted">Started on: </span>
                        {
                          ca.start_date
                            ?.replace("T", " - ")
                            .replace("Z", "")
                            .split(".")[0]
                        }
                        <br />
                        <span className="text-muted">Due on: </span>
                        {ca.due_date?.replace("T", " - ").replace("Z", "")}
                      </p>
                    </div>
                  ))}
              </div>
            </div>
          </div>
          <div className="mx-0 my-5 row align-items-center pb-5 indivCourseElement3">
            <div className="col-lg-12 col-sm-12 text-left mx-2 px-3 pt-3">
              <div className="px-4 row justify-content-between">
                <h1 className="mb-2 mt-3 text-white">
                  Resources
                  <button
                    type="button"
                    className="btn btn-warning mx-4"
                    data-toggle="modal"
                    data-target="#manageResourceForm"
                    onClick={handleAddNew}
                  >
                    <span>+</span> Add New
                  </button>
                </h1>
              </div>

              <hr className="mb-4" style={{ "background-color": "white" }} />
              {course?.resource &&
                course?.resource.length > 0 &&
                course?.resource?.map((cr, i) => (
                  <div className="col-lg-11 col-sm-12 text-left mx-auto p-2 px-3 mb-4 indivCourseAssignment">
                    <h3 className="my-2">
                      Lecture {i + 1}: {cr.title}
                      <button
                        type="button"
                        className="btn btn-sm btn-outline-info ml-4"
                        data-toggle="modal"
                        data-target="#manageResourceForm"
                        name={RESOURCE}
                        onClick={handleEdit}
                        value={cr.id}
                      >
                        Edit
                      </button>
                      <button
                        type="button"
                        className="btn btn-sm btn-outline-danger ml-4"
                        data-toggle="modal"
                        data-target="#confirmationModal"
                        onClick={() =>
                          setToDelete({ type: RESOURCE, id: cr.id })
                        }
                      >
                        Delete
                      </button>
                    </h3>
                    <p>{cr.description}</p>
                  </div>
                ))}
            </div>
          </div>
        </>
      )}
      <ModalBox
        id="manageResourceForm"
        title={updateResourceData ? "Update Resource" : "Add New Resource"}
      >
        <TeacherResourceForm
          updateData={updateResourceData}
          onComplete={() => setResourceList()}
          course={course?.id || -1}
        />
      </ModalBox>
      <ModalBox
        id="manageAssignmentForm"
        title={
          updateAssignmentData ? "Update Assignment" : "Add New Assignment"
        }
      >
        <TeacherAssignmentForm
          updateData={updateAssignmentData}
          onComplete={() => setAssignmentList()}
          course={course?.id || -1}
        />
      </ModalBox>
      <ConfirmModalBox toogleConfirmation={toogleConfirmation} />
    </>
  );
};
