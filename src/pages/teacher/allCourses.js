import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { TeachersContext } from "../../components/hooks/useTeacherContext";

export const TeacherCourses = () => {
  const { assignedCourses } = useContext(TeachersContext);

  return (
    <>
      <div className="m-0 row align-items-center pb-5 indivCourseElement2">
        <div
          className="col-lg-12 col-sm-12 text-left mx-2 px-3 pt-3"
          style={{ "min-height": "700px" }}
        >
          <div className="px-4 row justify-content-between">
            <h1 className="mb-2 mt-3 text-white">Courses</h1>
          </div>
          <hr className="mb-4" style={{ "background-color": "white" }} />
          {assignedCourses && assignedCourses.length > 0 ? (
            assignedCourses.map((ec) => (
              <div className="col-lg-11 col-sm-12 text-left mx-auto p-2 px-3 mb-4 indivCourseAssignment">
                <NavLink
                  to={`/teacher/courses/${ec.id}`}
                  style={{ color: "black" }}
                  activeClassName="active"
                >
                  <h2 className="mt-3">
                    Title: {ec.title}
                    {ec.resource && ec.resource.length > 0 ? (
                      <span className="badge bg-success mx-4 py-2">
                        HAS RESOURCES
                      </span>
                    ) : (
                      <span className="badge bg-danger mx-4 py-2">
                        NO RESOURCES
                      </span>
                    )}
                    {ec.assignment && ec.assignment.length > 0 ? (
                      <span className="badge bg-success mx-4 py-2">
                        HAS ASSIGNMENTS
                      </span>
                    ) : (
                      <span className="badge bg-warning mx-4 py-2">
                        NO ASSIGNMENTS
                      </span>
                    )}
                  </h2>
                  <p className="my-4 ml-5 indivCourseOverviewText">
                    {ec.description}
                  </p>
                  <p className="mb-3 indivCourseOverviewText">
                    <span className="text-muted">Batch: </span>
                    {ec.batch}
                  </p>
                </NavLink>
              </div>
            ))
          ) : (
            <div>
              You have not been assinged to any course.
              <br />
              Please contact administrator if you this is an error.
            </div>
          )}
        </div>
      </div>
    </>
  );
};
