import React, { useContext, useEffect, useState } from "react";
import { ConfirmModalBox } from "../../components/coreUI/Modal/PredefineModals";
import { DataContext } from "../../components/hooks/useDataContext";
import { assignment } from "../../utils/mockdata";

export const TeacherAssignments = () => {
  const { courseList } = useContext(DataContext);

  const [courseWithAssignment, setCourseWithAssignment] = useState([]);

  useEffect(() => {
    const _courseWithAssignment = courseList.map((ec) => ({
      assignment,
      courseName: courseList.filter((cl) => cl.id === ec)[0].title,
      courseBatch: courseList.filter((cl) => cl.id === ec)[0].batch,
    }));
    setCourseWithAssignment(_courseWithAssignment);
  }, [courseList]);

  return (
    <>
      {courseWithAssignment.map((cwa) => (
        <>
          <div className="mx-5 my-5 text-left">
            <h1 className="">{cwa.courseName}</h1>
            <h4 className="">Batch: {cwa.courseBatch}</h4>
          </div>
          {cwa.assignment.map((cass) => (
            <div
              className="mx-5 my-5 row align-items-center p-1 IndivCourseElement1"
              style={{ "background-color": "white", "border-radius": "25px" }}
            >
              <div className="col-lg-12 col-sm-12 text-left mx-2 px-3">
                <h1 className="mb-2 mt-3">Assignment: {cass.title}</h1>
                <hr />
                <p className="mb-3 indivCourseOverviewText">
                  {cass.description}
                </p>
                <p className="mb-3 indivCourseOverviewText">
                  Started on: {cass.start_date}
                  <br />
                  Due on: {cass.due_date}
                </p>
              </div>
              <div className="row mx-auto d-flex justify-content-around">
                <div className="form-group col-5">
                  <button
                    type="button"
                    className="btn btn-lg btn-info btn-block border-0 mx-auto"
                    style={{ width: "200px" }}
                  >
                    Edit
                  </button>
                </div>
                <div className="form-group col-5">
                  <button
                    type="button"
                    className="btn btn-lg btn-danger btn-block border-0 mx-auto"
                    style={{ width: "200px" }}
                    data-target="#confirmationModal"
                    data-toggle="modal"
                  >
                    Delete
                  </button>
                </div>
              </div>
            </div>
          ))}
        </>
      ))}
      <ConfirmModalBox toogleConfirmation={(e) => console.error(e)} />
    </>
  );
};
