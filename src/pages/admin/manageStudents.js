import React, { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { AdminStudentForm } from "../../components/admin/form/studentForm";
import ModalBox from "../../components/coreUI/Modal";
import { ConfirmModalBox } from "../../components/coreUI/Modal/PredefineModals";
import { DataContext } from "../../components/hooks/useDataContext";
import { requestDeleteStudent } from "../../lib/studentApi";

const studentFormModalID = "manageStudentForm";

export const ManageStudents = () => {
  const { studentList, batchList, setStudentList } = useContext(DataContext);

  const [updateData, setUpdateData] = useState(null);
  const [toDelete, setToDelete] = useState(null);

  const handleEdit = (e) => {
    const { value } = e.target;
    const data = studentList.filter((tl) => tl.id.toString() === value)[0];
    setUpdateData({
      first_name: data.first_name,
      last_name: data.last_name,
      email: data.user__email,
      username: data.user__username,
      user_batch: data.user__user_batch,
      id: data.id,
    });
  };

  const handleAddNew = () => {
    setUpdateData(null);
  };

  const toogleConfirmation = (val) => {
    if (val) {
      handleDelete();
    }
  };

  const handleDelete = async () => {
    const res = await requestDeleteStudent(toDelete);
    if (!res || res.error) {
      toast.error(res.error);
    } else {
      toast.success("Successful");
      setStudentList();
    }
  };

  return (
    <>
      <div>
        <div className="mx-5 my-5 text-left">
          <h1>Manage Students</h1>
        </div>
        <div className="mx-5 my-5 px-5 w-50 row align-items-center justify-content-between manageteacherBanner">
          <h2>Add new students</h2>
          <button
            type="button"
            className="btn btn-lg btn-success"
            onClick={handleAddNew}
            data-toggle="modal"
            data-target={`#${studentFormModalID}`}
          >
            Add New
          </button>
        </div>
      </div>
      <div className="m-0 row align-items-center pb-5 indivCourseElement2">
        <div
          className="col-lg-12 col-sm-12 text-left mx-2 px-3 pt-3"
          style={{ "min-height": "700px" }}
        >
          <div className="px-4 row justify-content-between">
            <h1 className="mb-2 mt-3 text-white">Students</h1>
          </div>

          <hr className="mb-4" style={{ "background-color": "white" }} />
          {studentList.length > 0 ? (
            <div className="col-lg-11 col-sm-12 text-left mx-auto mb-4 p-0 indivCourseAssignment">
              <table className="table myTable m-0">
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">SN</th>
                    <th scope="col">Full name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Username</th>
                    <th scope="col">Batch</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {studentList.map((tl, i) => (
                    <tr>
                      <td>{i + 1}</td>
                      <td>
                        {tl.first_name} {tl.last_name}
                      </td>
                      <td>{tl.user__email}</td>
                      <td>{tl.user__username}</td>
                      <td>
                        {batchList.length > 0
                          ? batchList.filter(
                              (b) =>
                                parseInt(b.id) === parseInt(tl.user__user_batch)
                            )[0].batch
                          : ""}
                      </td>
                      <td>
                        <div className="row justify-content-around">
                          <button
                            type="button"
                            name="edit"
                            value={tl.id}
                            className="btn btn-outline-info btn-sm"
                            onClick={handleEdit}
                            data-toggle="modal"
                            data-target={`#${studentFormModalID}`}
                          >
                            Edit
                          </button>
                          <button
                            type="button"
                            name="delete"
                            className="btn btn-outline-danger btn-sm"
                            onClick={() => setToDelete(tl.user__username)}
                            data-toggle="modal"
                            data-target="#confirmationModal"
                          >
                            Delete
                          </button>
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          ) : (
            <div>No Students to show.</div>
          )}
        </div>
      </div>
      <ModalBox
        id="manageStudentForm"
        title={updateData ? "Update Student" : "Add New Student"}
      >
        <AdminStudentForm
          updateData={updateData}
          onComplete={() => setStudentList()}
          batches={batchList}
        />
      </ModalBox>
      <ConfirmModalBox toogleConfirmation={toogleConfirmation} />
    </>
  );
};
