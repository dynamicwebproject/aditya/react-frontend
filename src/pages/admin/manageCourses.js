import React, { useContext, useState } from "react";
import { toast } from "react-toastify";
import { AdminCourseForm } from "../../components/admin/form/courseForm";
import ModalBox from "../../components/coreUI/Modal";
import { ConfirmModalBox } from "../../components/coreUI/Modal/PredefineModals";
import { DataContext } from "../../components/hooks/useDataContext";
import { requestDeleteCourse, requestGetAllCourses } from "../../lib/courseApi";

const courseModalID = "manageTeacherForm";

export const ManageCourses = () => {
  const { courseList, batchList, teacherList, setCourseList } = useContext(
    DataContext
  );
  const [updateData, setUpdateData] = useState(null);
  const [toDelete, setToDelete] = useState(null);

  const onComplete = () => {
    setCourseList();
  };

  const handleEdit = (e) => {
    const { value } = e.target;
    const data = courseList.filter((cl) => cl.id.toString() === value)[0];
    setUpdateData({
      code: data.code,
      title: data.title,
      description: data.description,
      batch: data.batch,
      teacher: data.teacher,
      id: data.id,
    });
  };

  const handleAddNew = () => {
    setUpdateData(null);
  };

  const toogleConfirmation = (val) => {
    if (val) {
      handleDelete();
    }
  };

  const handleDelete = async () => {
    const res = await requestDeleteCourse(toDelete);
    if (!res || res.error) {
      toast.error(res?.error || "");
    } else {
      toast.success(res || "Successful");
      onComplete();
    }
  };

  return (
    <>
      <div>
        <div className="mx-5 my-5 text-left">
          <h1 className="">Manage Courses</h1>
        </div>
        <div className="mx-5 my-5 px-5 w-50 row align-items-center justify-content-between manageteacherBanner">
          <h2>Add new courses</h2>
          <button
            type="button"
            className="btn btn-lg btn-success"
            onClick={handleAddNew}
            data-toggle="modal"
            data-target={`#${courseModalID}`}
          >
            Add New
          </button>
        </div>
      </div>
      <div className="m-0 row align-items-center pb-5 indivCourseElement2">
        <div
          className="col-lg-12 col-sm-12 text-left mx-2 px-3 pt-3"
          style={{ "min-height": "700px" }}
        >
          <div className="px-4 row justify-content-between">
            <h1 className="mb-2 mt-3 text-white">Courses</h1>
          </div>

          <hr className="mb-4" style={{ "background-color": "white" }} />
          {courseList.length > 0 ? (
            <div className="col-lg-11 col-sm-12 text-left mx-auto mb-4 p-0 indivCourseAssignment">
              <table className="table myTable m-0">
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">SN</th>
                    <th scope="col">Course code</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Batch</th>
                    <th scope="col">Teacher</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {courseList.map((cl, i) => (
                    <tr>
                      <td>{i + 1}</td>
                      <td>{cl.code}</td>
                      <td>{cl.title}</td>
                      <td>{cl.description}</td>
                      <td>{getBatchName(batchList, cl.batch)}</td>
                      <td>{getTeacherName(teacherList, cl.teacher)}</td>
                      <td>
                        <div className="row justify-content-around">
                          <button
                            type="button"
                            name="edit"
                            value={cl.id}
                            className="btn btn-outline-info btn-sm"
                            onClick={handleEdit}
                            data-toggle="modal"
                            data-target={`#${courseModalID}`}
                          >
                            Edit
                          </button>
                          <button
                            type="button"
                            name="delete"
                            className="btn btn-outline-danger btn-sm"
                            onClick={() => setToDelete(cl.id)}
                            data-toggle="modal"
                            data-target="#confirmationModal"
                          >
                            Delete
                          </button>
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          ) : (
            <div>No courses to show.</div>
          )}
        </div>
      </div>
      <ModalBox id={courseModalID} title={"Add new course"}>
        <AdminCourseForm
          batches={batchList}
          teachers={teacherList}
          updateData={updateData}
          onComplete={onComplete}
        />
      </ModalBox>
      <ConfirmModalBox toogleConfirmation={toogleConfirmation} />
    </>
  );
};

const getTeacherName = (teachers = [], id) => {
  if (teachers.length === 0 || !id) {
    return "";
  }
  const teacher = teachers.filter((tch) => tch.id === id)[0];
  return `${teacher.first_name} ${teacher.last_name}`;
};

const getBatchName = (batches = [], id) => {
  if (batches.length === 0 || !id) {
    return "";
  }
  const batch = batches.filter((bch) => bch.id === id)[0];
  return `${batch.batch}`;
};
