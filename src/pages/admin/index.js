export * from "./dashboard";
export * from "./manageCourses";
export * from "./manageStudents";
export * from "./manageTeacher";
