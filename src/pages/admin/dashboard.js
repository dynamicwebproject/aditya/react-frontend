import React, { useContext } from "react";
import { DataContext } from "../../components/hooks/useDataContext";

export const AdminDashboard = () => {
  const { counts } = useContext(DataContext);
  return (
    <>
      <div
        className="mx-auto my-5 text-left py-4 col-lg-12 col-sm-12"
        style={{ "background-color": "white" }}
      >
        <h1 className="font-weight-bolder mx-5">Overview</h1>
        <div className="mx-auto row my-5 justify-content-around align-items-center text-center">
          <div className="py-3 mb-3 econStatCard1 text-center">
            <h2>Students</h2>
            <hr />
            <h1 className="font-weight-bolder">{counts.student}</h1>
          </div>
          <div className="py-3 mb-3 econStatCard2 text-center">
            <h2>Courses</h2>
            <hr />
            <h1 className="font-weight-bolder">{counts.course}</h1>
          </div>
          <div className="py-3 mb-3 econStatCard3 text-center">
            <h2>Teachers</h2>
            <hr />
            <h1 className="font-weight-bolder">{counts.teacher}</h1>
          </div>
        </div>
      </div>
    </>
  );
};
