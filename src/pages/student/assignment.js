import React, { useContext } from "react";
import ModalBox from "../../components/coreUI/Modal";
import { CoursesContext } from "../../components/hooks/useCourseContext";
import { SubmitAssignmentForm } from "../../components/student/form/submitForm";
import { assignment } from "../../utils/mockdata";

export const AssignmentPage = (props) => {
  const { coursesList } = useContext(CoursesContext);
  const courseWithAssignment = coursesList?.map((ec) => {
    console.log(ec.batch)
    if (ec.batch === 2)
      return {
        assignment,
        courseName: ec.title,
        courseBatch: ec.batch,
      };
  });

  return (
    <>
      {courseWithAssignment?.map((cwa) => (
        <>
          <div className="mx-5 my-5 text-left">
            <h1 className="">{cwa?.courseName}</h1>
            <h4 className="">Batch: {cwa?.courseBatch}</h4>
          </div>
          {cwa?.assignment.map((cass) => (
            <div
              className="mx-5 my-5 row align-items-center p-1 IndivCourseElement1"
              style={{ "background-color": "white", "border-radius": "25px" }}
            >
              <div className="col-lg-12 col-sm-12 text-left mx-2 px-3">
                <h1 className="mb-2 mt-3">Assignment: {cass.title}</h1>
                <hr />
                <p className="mb-3 indivCourseOverviewText">
                  {cass.description}
                </p>
                <p className="mb-3 indivCourseOverviewText">
                  Started on: {cass.startDate}
                  <br />
                  Due on: {cass.dueDate}
                </p>
              </div>
              <div className="row mx-auto d-flex justify-content-around">
                <div className="form-group col-5">
                  <button
                    type="button"
                    className="btn btn-lg btn-warning btn-block border-0 mx-auto"
                    style={{ width: "200px" }}
                    data-toggle="modal"
                    data-target="#submitAssignmentModal"
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          ))}
        </>
      ))}
      <ModalBox id="submitAssignmentModal" title="Submit Assignment">
        <SubmitAssignmentForm />
      </ModalBox>
    </>
  );
};
