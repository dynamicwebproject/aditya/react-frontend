import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { CoursesContext } from "../../components/hooks/useCourseContext";
import { StudentsContext } from "../../components/hooks/useStudentContext";
import { UserContext } from "../../components/hooks/useUserContext";

export const AllCourses = () => {
  const { assignedCourses } = useContext(StudentsContext);

  return (
    <>
      <div className="m-0 row align-items-center pb-5 indivCourseElement2">
        <div
          className="col-lg-12 col-sm-12 text-left mx-2 px-3 pt-3"
          style={{ "min-height": "700px" }}
        >
          <div className="px-4 row justify-content-between">
            <h1 className="mb-2 mt-3 text-white">Courses</h1>
          </div>
          <hr className="mb-4" style={{ "background-color": "white" }} />
          {assignedCourses && assignedCourses.length > 0 ? (
            assignedCourses.map((ec) => (
              <div className="col-lg-11 col-sm-12 text-left mx-auto p-2 px-3 mb-4 indivCourseAssignment">
                <Link
                  to={`/courses/${ec.id}`}
                  style={{ "text-decoration": "none", color: "black" }}
                >
                  <h3 className="mb-2 mt-3">
                    {ec.code}: {ec.title}
                  </h3>
                  <p className="mb-2 indivCourseOverviewText">
                    {ec.description}
                  </p>
                  <p className="mb-3 indivCourseOverviewText">
                    Batch: {ec.batch}
                  </p>
                </Link>
              </div>
            ))
          ) : (
            <div>
              You have not been assigned with any courses. <br /> If you think
              this is an error please contact administrator.
            </div>
          )}
        </div>
      </div>
    </>
  );
};
