import React, { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { DataContext } from "../../components/hooks/useDataContext";
import { TeacherResourceForm } from "../../components/teacher/resourceForm";
import ModalBox from "../../components/coreUI/Modal/index";
import { StudentsContext } from "../../components/hooks/useStudentContext";
import { SubmitAssignmentForm } from "../../components/student/form/submitForm";
import { UserContext } from "../../components/hooks/useUserContext";

export const StudentCoursePage = (props) => {
  const courseId = props.match.params.id;
  const { userDetail } = useContext(UserContext);

  const { assignedCourses } = useContext(StudentsContext);
  const { setAssignmentList } = useContext(DataContext);
  const [updateResourceData, setUpdateResourceData] = useState(null);
  const [submitId, setSubmitId] = useState(-1);
  const [course, setCourse] = useState(null);

  useEffect(() => {
    if (assignedCourses && assignedCourses.length > 0) {
      setCourse(
        assignedCourses.filter((ec) => ec.id === parseInt(courseId))[0]
      );
    }
  }, [assignedCourses]);

  return (
    <>
      {course && (
        <>
          <div className="mx-5 my-5 text-left">
            <h1 className="">{course.title}</h1>
            <h4 className="">
              <span className="text-muted">Batch:</span> {course.batch}
            </h4>
          </div>
          <div
            className="mx-5 my-5 row align-items-center p-1 IndivCourseElement1"
            style={{ "background-color": "white", "border-radius": "25px" }}
          >
            <div className="col-lg-12 col-sm-12 text-left mx-2 px-3">
              <h1 className="mb-2 mt-3">Overview</h1>
              <hr />
              <p className="mb-5 indivCourseOverviewText">
                {course.description}
              </p>
            </div>
          </div>
          <div className="mx-0 my-5 row align-items-center pb-5 indivCourseElement2">
            <div className="col-lg-12 col-sm-12 text-left mx-2 px-3 pt-3">
              <div className="px-4 row justify-content-between">
                <h1 className="mb-2 mt-3 text-white">Assignments</h1>
              </div>
              <hr className="mb-4" style={{ "background-color": "white" }} />
              <div className="row align-items-center">
                {course?.assignment &&
                  course?.assignment.length > 0 &&
                  course?.assignment?.map((ca) => (
                    <div className="col-lg-3 col-sm-3 mx-5 text-left p-2 px-3 mb-4 indivCourseAssignment">
                      <h3 className="mb-2 mt-3">
                        {ca.title}
                        <button
                          type="button"
                          className="btn btn-warning mx-4"
                          data-toggle="modal"
                          data-target="#submitAssignmentModal"
                          onClick={() => setSubmitId(ca.id)}
                        >
                          Submit
                        </button>
                      </h3>
                      <p className="mb-3 indivCourseDescription indivCourseOverviewText">
                        {ca.description}
                      </p>
                      <p className="mb-3 indivCourseOverviewText">
                        <span className="text-muted">Started on: </span>
                        {
                          ca.start_date
                            ?.replace("T", " - ")
                            .replace("Z", "")
                            .split(".")[0]
                        }
                        <br />
                        <span className="text-muted">Due on: </span>
                        {ca.due_date?.replace("T", " - ").replace("Z", "")}
                      </p>
                    </div>
                  ))}
              </div>
            </div>
          </div>
          <div className="mx-0 my-5 row align-items-center pb-5 indivCourseElement3">
            <div className="col-lg-12 col-sm-12 text-left mx-2 px-3 pt-3">
              <div className="px-4 row justify-content-between">
                <h1 className="mb-2 mt-3 text-white">Resources</h1>
              </div>
              <hr className="mb-4" style={{ "background-color": "white" }} />
              {course?.resource &&
                course?.resource.length > 0 &&
                course?.resource?.map((cr, i) => (
                  <div className="col-lg-11 col-sm-12 text-left mx-auto p-2 px-3 mb-4 indivCourseAssignment">
                    <h3 className="my-2">
                      Title {i + 1}: {cr.title}
                    </h3>
                    <p>{cr.description}</p>
                  </div>
                ))}
            </div>
          </div>
        </>
      )}
      <ModalBox id="submitAssignmentModal" title="Submit assignment">
        <SubmitAssignmentForm
          assignment={submitId}
          author={userDetail?.student_id || -1}
        />
      </ModalBox>
    </>
  );
};
