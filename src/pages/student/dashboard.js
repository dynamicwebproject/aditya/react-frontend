import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { StudentsContext } from "../../components/hooks/useStudentContext";
import { UserContext } from "../../components/hooks/useUserContext";

export const StudentDashboard = (props) => {
  const { userDetail } = useContext(UserContext);
  const { assignedCourses } = useContext(StudentsContext);

  return (
    <>
      <div className="px-5 mt-0">
        <div className="mx-auto mb-5 mt-3 row align-items-center homeElement1">
          <div className="col-lg-5 col-sm-12 text-left row homeElement3t m-0 justify-content-center align-items-center">
            <div className="homeHeadingText">
              <h1 className="mb-4 text-center bigText">My Info</h1>
            </div>
          </div>
          <div className="h3 px-5 mt-2">
            {userDetail && (
              <>
                <p>Username: {userDetail?.username || ""}</p>
                <p>First Name: {userDetail?.first_name || ""}</p>
                <p>Last Name: {userDetail?.last_name || ""}</p>
                <p>Email: {userDetail?.email || ""}</p>
                <p>
                  <span className="badge bg-success">
                    {userDetail.user_type.toUpperCase() || ""}
                  </span>
                </p>
              </>
            )}
          </div>
        </div>

        <div className="mx-auto my-5 row align-items-center homeElement2">
          <div
            className="col-lg-6 col-sm-11 text-center m-auto"
            style={{ color: "white" }}
          >
            <h1 className="mb-2 mx-2 quoteText">
              Education is the kindling of a flame, not the filling of a vessel
            </h1>
            <p>-Socrates</p>
          </div>
        </div>

        <div className="mx-auto row my-5 justify-content-center align-items-center text-center">
          <h1 className="">ASSIGNED COURSES</h1>
        </div>
        <div className="mx-auto row my-5 justify-content-around align-items-center text-center">
          {assignedCourses && assignedCourses.length > 0 ? (
            assignedCourses.map((ec) => (
              <NavLink
                to={`/teacher/courses/${ec.id}`}
                style={{ color: "black" }}
                className="py-3 mb-3 indivLecturerCard"
                activeClassName="active"
              >
                <div className="w-80 mx-auto my-3" style={{ width: "200px" }}>
                  <img
                    className="w-100 h-100 rounded-circle"
                    src={"/images/img1.jpg"}
                    alt="lecturer's piture"
                  />
                </div>
                <h3>
                  <strong>{ec.title}</strong>
                </h3>
                <p className="mb-3 indivCourseOverviewText">
                  <span className="text-muted">Batch: </span>
                  {ec.batch}
                </p>
                <div className="d-flex flex-column">
                  {ec.resource && ec.resource.length > 0 ? (
                    <span className="badge bg-success my-2 py-2">
                      HAS RESOURCES
                    </span>
                  ) : (
                    <span className="badge bg-danger my-2 py-2">
                      NO RESOURCES
                    </span>
                  )}
                  {ec.assignment && ec.assignment.length > 0 ? (
                    <span className="badge bg-success my-2 py-2">
                      HAS ASSIGNMENTS
                    </span>
                  ) : (
                    <span className="badge bg-warning my-2 py-2">
                      NO ASSIGNMENTS
                    </span>
                  )}
                </div>
              </NavLink>
            ))
          ) : (
            <div>No course is assigned to you.</div>
          )}
        </div>
      </div>
    </>
  );
};
