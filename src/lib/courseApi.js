import sendRequest from "./sendRequest";

const BASE_PATH = "/course";

const requestGetAllCourses = async () => {
  const requestOptions = {
    method: "GET",
    secured: true,
  };

  const res = await sendRequest(`${BASE_PATH}/`, requestOptions);
  return res;
};

const requestAddCourse = async (courseDetail) => {
  const formData = new FormData();
  Object.keys(courseDetail).map((key) => {
    formData.set(key, courseDetail[key]);
  });
  const requestOptions = {
    method: "POST",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/`, requestOptions);
  return res;
};

const requestUpdateCourse = async (courseDetail, id) => {
  const formData = new FormData();
  Object.keys(courseDetail).map((key) => {
    formData.set(key, courseDetail[key]);
  });
  const requestOptions = {
    method: "PUT",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/${id}/`, requestOptions);
  return res;
};

const requestDeleteCourse = async (id) => {
  const requestOptions = {
    method: "DELETE",
    secured: true,
  };

  const res = await sendRequest(`${BASE_PATH}/${id}/`, requestOptions);
  return res.data;
};

export {
  requestGetAllCourses,
  requestAddCourse,
  requestUpdateCourse,
  requestDeleteCourse,
};
