import sendRequest from "./sendRequest";
const BASE_PATH = "/user";

const getUserDetail = async () => {
  const requestOptions = {
    method: "GET",
    secured: true,
  };

  const res = await sendRequest(`${BASE_PATH}/me/`, requestOptions);
  return res;
};

export { getUserDetail };
