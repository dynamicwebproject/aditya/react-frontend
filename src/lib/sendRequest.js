import axios from "axios";
import { errors } from "../utils/constants";

const port = process.env.PORT || 8000;

const api = axios.create({
  baseURL: `http://localhost:${port}/api`,
  responseType: "json",
});

const unsecureHeader = {
  "Content-type": "application/json",
};

const secureHeader = {
  "Content-type": "application/json",
  Authorization: `Bearer ${localStorage.getItem("token")}`,
};

const sendRequest = async (path, opts = { method: "POST", secured: false }) => {
  try {
    const response = await api.request({
      method: opts.method,
      url: path,
      headers: opts.secured ? secureHeader : unsecureHeader,
      timeout: 5000,
      ...opts,
    });
    const { data } = response;
    if (data?.error) {
      return { error: data.error };
    }
    return data;
  } catch (error) {
    return errorHandler(error);
  }
};

const errorHandler = (error) => {
  const _error = { error: errors.generic };
  if (error?.response?.data?.error) {
    _error.error = error?.response?.data?.error;
  }
  if (error?.response?.data?.error?.email) {
    _error.error = error?.response?.data?.error.email[0];
  }
  if (error?.response?.data?.error?.username) {
    _error.error = error?.response?.data?.error.username[0];
  }
  if (error?.response?.data?.code) {
    _error.error = error?.response?.data?.messages[0].message;
  }
  return _error;
};

export default sendRequest;
