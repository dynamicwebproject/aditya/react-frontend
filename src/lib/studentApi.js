import sendRequest from "./sendRequest";

const BASE_PATH = "/student";

const requestGetAllStudents = async () => {
  const requestOptions = {
    method: "GET",
    secured: true,
  };

  const res = await sendRequest(`${BASE_PATH}/`, requestOptions);
  return res;
};

const requestAddStudent = async (studentDetail) => {
  const formData = new FormData();
  Object.keys(studentDetail).map((key) => {
    formData.set(key, studentDetail[key]);
  });
  const requestOptions = {
    method: "POST",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/create/`, requestOptions);
  return res;
};

const requestUpdateStudent = async (studentDetail, id) => {
  const formData = new FormData();
  Object.keys(studentDetail).map((key) => {
    formData.set(key, studentDetail[key]);
  });
  const requestOptions = {
    method: "PUT",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/update/${id}/`, requestOptions);
  return res;
};

const requestDeleteStudent = async (username) => {
  const formData = new FormData();
  formData.set("username", username);
  const requestOptions = {
    method: "DELETE",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/delete/`, requestOptions);
  return res;
};

const requestFetchBatches = async () => {
  const requestOptions = {
    method: "GET",
    secured: true,
  };
  const res = await sendRequest("/batch/", requestOptions);
  return res;
};

const requestAssignmentSubmit = async (assignmentDetail) => {
  const formData = new FormData();
  Object.keys(assignmentDetail).map((key) => {
    formData.set(key, assignmentDetail[key]);
  });
  const requestOptions = {
    method: "POST",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`/submission/`, requestOptions);
  return res;
};

export {
  requestGetAllStudents,
  requestAddStudent,
  requestUpdateStudent,
  requestDeleteStudent,
  requestFetchBatches,
  requestAssignmentSubmit,
};
