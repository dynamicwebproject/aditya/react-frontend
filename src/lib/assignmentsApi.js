import sendRequest from "./sendRequest";

const BASE_PATH = "/assignment";

const requestGetAllAssignments = async () => {
  const requestOptions = {
    method: "GET",
    secured: true,
  };

  const res = await sendRequest(`${BASE_PATH}/`, requestOptions);
  return res;
};

const requestAddAssignment = async (assignmentDetail) => {
  const formData = new FormData();
  Object.keys(assignmentDetail).map((key) => {
    formData.set(key, assignmentDetail[key]);
  });
  const requestOptions = {
    method: "POST",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/`, requestOptions);
  return res;
};

const requestUpdateAssignment = async (assignmentDetail, id) => {
  const formData = new FormData();
  Object.keys(assignmentDetail).map((key) => {
    formData.set(key, assignmentDetail[key]);
  });
  const requestOptions = {
    method: "PUT",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/${id}/`, requestOptions);
  return res;
};

const requestDeleteAssignment = async (id) => {
  const requestOptions = {
    method: "DELETE",
    secured: true,
  };

  const res = await sendRequest(`${BASE_PATH}/${id}/`, requestOptions);
  return res;
};

export {
  requestGetAllAssignments,
  requestAddAssignment,
  requestUpdateAssignment,
  requestDeleteAssignment,
};
