import sendRequest from "./sendRequest";

const BASE_PATH = "/resources";

const requestGetAllResources = async () => {
  const requestOptions = {
    method: "GET",
    secured: true,
  };

  const res = await sendRequest(`${BASE_PATH}/`, requestOptions);
  return res;
};

const requestAddResource = async (resourceDetail) => {
  const formData = new FormData();
  Object.keys(resourceDetail).map((key) => {
    formData.set(key, resourceDetail[key]);
  });
  const requestOptions = {
    method: "POST",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/`, requestOptions);
  return res;
};

const requestUpdateResource = async (resourceDetail, id) => {
  const formData = new FormData();
  Object.keys(resourceDetail).map((key) => {
    formData.set(key, resourceDetail[key]);
  });
  const requestOptions = {
    method: "PUT",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/${id}/`, requestOptions);
  return res;
};

const requestDeleteResource = async (id) => {
  const requestOptions = {
    method: "DELETE",
    secured: true,
  };

  const res = await sendRequest(`${BASE_PATH}/${id}/`, requestOptions);
  return res;
};

export {
  requestGetAllResources,
  requestAddResource,
  requestUpdateResource,
  requestDeleteResource,
};
