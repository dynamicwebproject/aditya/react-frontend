import sendRequest from "./sendRequest";

const BASE_PATH = "/teacher";

const requestGetAllTeachers = async () => {
  const requestOptions = {
    method: "GET",
    secured: true,
  };

  const res = await sendRequest(`${BASE_PATH}/`, requestOptions);
  return res;
};

const requestAddTeacher = async (teacherDetail) => {
  const formData = new FormData();
  Object.keys(teacherDetail).map((key) => {
    formData.set(key, teacherDetail[key]);
  });
  const requestOptions = {
    method: "POST",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/create/`, requestOptions);
  return res;
};

const requestUpdateTeacher = async (teacherDetail, id) => {
  const formData = new FormData();
  Object.keys(teacherDetail).map((key) => {
    formData.set(key, teacherDetail[key]);
  });
  const requestOptions = {
    method: "PUT",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/update/${id}/`, requestOptions);
  return res;
};

const requestDeleteTeacher = async (username) => {
  const formData = new FormData();
  formData.set("username", username);
  const requestOptions = {
    method: "DELETE",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/delete/`, requestOptions);
  return res;
};

export {
  requestGetAllTeachers,
  requestAddTeacher,
  requestUpdateTeacher,
  requestDeleteTeacher,
};
