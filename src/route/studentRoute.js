import React from "react";
import { Route, Redirect } from "react-router-dom";

import { Footer, NavBar } from "../components/coreUI";
import { useRole } from "../components/hooks/useRole";
import StudentsProvider from "../components/hooks/useStudentContext";

const StudentRoute = ({ component: Component, ...data }) => {
  const { isStudent } = useRole();
  return (
    <>
      <Route
        {...data}
        render={(props) => {
          return localStorage.getItem("token") ? (
            isStudent ? (
              <>
                <NavBar {...props} />
                <div>
                  <StudentsProvider>
                    <Component {...props} state={data.state} />
                  </StudentsProvider>
                </div>
                <Footer {...props} />
              </>
            ) : (
              <Redirect to="/" />
            )
          ) : (
            <>
              {!isStudent && window.localStorage.clear()}
              <Redirect to="/" />
            </>
          );
        }}
      />
    </>
  );
};

export default StudentRoute;
