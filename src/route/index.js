import React, { useContext, useEffect } from "react";
import { Redirect, Route, Switch } from "react-router-dom";

import AuthRoute from "./authRoute";
import StudentRoute from "./studentRoute";
import AdminRoute from "./adminRoute";
import TeacherRoute from "./teacherRoute";

import Login from "../pages/auth/login";
import ForgotPassword from "../pages/auth/forgotPassword";
import ResetPassword from "../pages/auth/resetPassword";

import {
  StudentDashboard,
  StudentCoursePage,
  ResourcePage,
  AssignmentPage,
  AllCourses,
} from "../pages/student";

import {
  AdminDashboard,
  ManageCourses,
  ManageStudents,
  ManageTeachers,
} from "../pages/admin";

import {
  TeacherAssignment,
  TeacherAssignments,
  TeacherCoursePage,
  TeacherCourses,
  TeacherDashboard,
} from "../pages/teacher";
import { useRole } from "../components/hooks/useRole";

const AppRouting = () => {
  const { isAdmin, isStudent, isTeacher } = useRole();
  function NotFound() {
    return (
      <>
        <h1>Error 404!</h1>
        <h5>Page Not Found.</h5>
      </>
    );
  }
  console.log(isAdmin);

  return (
    <>
      <Switch>
        <Redirect
          exact
          from="/"
          to={
            isAdmin
              ? "/admin/dashboard"
              : isTeacher
              ? "/teacher/dashboard"
              : isStudent
              ? "/dashboard"
              : "/login"
          }
        />
        {/*UNPROTECTED ROUTE*/}
        <AuthRoute exact path="/login" component={Login} />
        <AuthRoute exact path="/forgot-password" component={ForgotPassword} />
        <AuthRoute
          exact
          path="/reset-password/:token"
          component={ResetPassword}
        />

        {/*STUDENT ROUTES*/}
        <StudentRoute exact path="/dashboard" component={StudentDashboard} />
        <StudentRoute exact path="/courses" component={AllCourses} />
        <StudentRoute exact path="/courses/:id" component={StudentCoursePage} />
        <StudentRoute
          exact
          path="/course/:id/:resourceId"
          component={ResourcePage}
        />
        <StudentRoute exact path="/assignments" component={AssignmentPage} />
        <StudentRoute
          exact
          path="/assignment/:assignmentId"
          component={AssignmentPage}
        />

        {/*ADMIN ROUTES*/}
        <AdminRoute exact path="/admin/dashboard" component={AdminDashboard} />
        <AdminRoute exact path="/admin/teachers" component={ManageTeachers} />
        <AdminRoute exact path="/admin/students" component={ManageStudents} />
        <AdminRoute exact path="/admin/courses" component={ManageCourses} />

        {/*TEACHER ROUTES*/}
        <TeacherRoute
          exact
          path="/teacher/dashboard"
          component={TeacherDashboard}
        />
        <TeacherRoute
          exact
          path="/teacher/courses"
          component={TeacherCourses}
        />
        <TeacherRoute
          exact
          path="/teacher/courses/:id"
          component={TeacherCoursePage}
        />
        <TeacherRoute
          exact
          path="/teacher/assignments"
          component={TeacherAssignments}
        />
        <TeacherRoute
          exact
          path="/teacher/assignments/:id"
          component={TeacherAssignment}
        />
        <Route component={NotFound} />
      </Switch>
    </>
  );
};

export default AppRouting;
