import React, { useContext, useEffect } from "react";
import { Route, Redirect } from "react-router-dom";
import { NavBar } from "../components/coreUI";
import { useRole } from "../components/hooks/useRole";

const AdminRoute = ({ component: Component, ...data }) => {
  const { isAdmin } = useRole();
  return (
    <>
      <Route
        {...data}
        render={(props) => {
          return localStorage.getItem("token") ? (
            isAdmin ? (
              <>
                <NavBar {...props} />
                <div>
                  <Component {...props} state={data.state} />
                </div>
                {/* ADD FOOTER */}
              </>
            ) : (
              <Redirect to="/" />
            )
          ) : (
            <>
              {window.localStorage.clear()}
              <Redirect to="/" />
            </>
          );
        }}
      />
    </>
  );
};

export default AdminRoute;
