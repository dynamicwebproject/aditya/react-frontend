import React from "react";
import { Route, Redirect } from "react-router-dom";

import { Footer, NavBar } from "../components/coreUI";
import { useRole } from "../components/hooks/useRole";
import TeachersProvider from "../components/hooks/useTeacherContext";

const TeacherRoute = ({ component: Component, ...data }) => {
  const { isTeacher } = useRole();

  return (
    <>
      <Route
        {...data}
        render={(props) => {
          return localStorage.getItem("token") ? (
            isTeacher ? (
              <>
                <NavBar {...props} />
                <div>
                  <TeachersProvider>
                    <Component {...props} state={data.state} />
                  </TeachersProvider>
                </div>
                <Footer {...props} />
              </>
            ) : (
              <Redirect to="/" />
            )
          ) : (
            <>
              {!isTeacher && window.localStorage.clear()}
              <Redirect to="/" />
            </>
          );
        }}
      />
    </>
  );
};

export default TeacherRoute;
