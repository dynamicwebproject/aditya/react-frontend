import React from "react";
import { Redirect, Route } from "react-router-dom";
import { useRole } from "../components/hooks/useRole";

const AuthRoute = ({ component: Component, ...data }) => {
  const { isTeacher, isStudent, isAdmin } = useRole();
  return (
    <>
      <Route
        {...data}
        render={(props) => {
          return isAdmin || isStudent || isTeacher ? (
            <Redirect to="/" />
          ) : (
            <>
              <div className="authContentWrapper position-relative overflow-hidden py-0 px-5 px-md-5 m-0 text-center">
                <div className="my-4 row p-0 justify-content-center">
                  <div className="col-lg-2"></div>
                  <div
                    className="container row col-lg-8 p-0 shadow"
                    style={{ "background-color": "white" }}
                  >
                    <div className="col-lg-6 p-0 m-0 align-content-center">
                      <div className="w-100 py-5 h-100 rounded-0 d-flex flex-column text-left align-items-center justify-content-center">
                        <Component {...props} />
                      </div>
                    </div>

                    <div className="col-lg-6 p-0 m-0 loginPic">
                      <div className="text-center p-0 m-auto rounded-0">
                        <header className="text-center py-4 position-absolute w-100 lmsTitle">
                          LEARNING MANAGEMENT SYSTEM
                        </header>
                        <img
                          className="w-100 h-100"
                          src={"/images/nonauthimage.png"}
                          alt="a happy picture"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-2"></div>
                </div>
              </div>
            </>
          );
        }}
      />
    </>
  );
};

export default AuthRoute;

// {/* <header className='text-center py-4'>LEARNING MANAGEMENT SYSTEM</header>
//               <div className="container bg-secondary text-center p-4">

//               </div>
//               <footer className="text-center fixed-bottom bg-secondary py-2">Copyright @LMS</footer> */}
